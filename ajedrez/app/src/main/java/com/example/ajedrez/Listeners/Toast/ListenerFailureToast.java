package com.example.ajedrez.Listeners.Toast;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class ListenerFailureToast implements OnFailureListener {

    private Context context;
    private String successMessage;

    public ListenerFailureToast(Context context, String successMessage){
        this.context = context;
        this.successMessage = successMessage;
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Toast.makeText(context, successMessage, Toast.LENGTH_SHORT).show();
    }
}
