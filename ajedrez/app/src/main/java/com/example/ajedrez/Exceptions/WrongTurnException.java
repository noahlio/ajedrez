package com.example.ajedrez.Exceptions;

public class WrongTurnException extends IllegalArgumentException{
    public WrongTurnException() {
        super("No es tu turno");
    }

    public WrongTurnException(String s) {
        super(s);
    }
}
