package com.example.ajedrez.Listeners;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.ajedrez.Activities.MyActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivity;

public class ListenerAbandonarPartida extends ListenerActivity {

    private PartidaActivity activity;
    public ListenerAbandonarPartida(PartidaActivity activity, Class cls, Context context, boolean destroy){
        super(activity, cls, context, destroy);
        this.activity = activity;
    }

    public void close(){
        activity.loseOnline();
        super.close();
    }
}