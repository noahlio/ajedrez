package com.example.ajedrez.VO;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Partida implements IPartida{

    protected String Id;
    protected Long idJuego;
    protected Map<String, List<?>> jugadas;

    public Partida(){
        this.jugadas = new LinkedHashMap<String, List<?>>();
    }

    public Partida(Partida partida){
        this.Id = partida.getId();
        this.idJuego = partida.getIdJuego();
        this.jugadas = partida.getJugadas();
    }

    //=================================== FUNCIONES COMPROBACION ===================================

    public boolean gameReady(){
        return true;
    }

    //=================================== FUNCIONES GETTER/SETTER ===================================

    public void setJugadas(Map<String, List<?>> jugadas) {
        this.jugadas = jugadas;
    }

    public Map<String, List<?>> getJugadas(){
        return this.jugadas;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getId(){
        return this.Id;
    }

    public void setIdJuego(Long IdJuego) {
        this.idJuego = IdJuego;
    }

    public Long getIdJuego(){
        return this.idJuego;
    }

    public Map<String, Object> toMap(){
        Map<String, Object> collection = new HashMap<String, Object>();
        collection.put("idJuego", this.idJuego);
        collection.put("jugadas", this.jugadas);
        return collection;
    }
}
