package com.example.ajedrez.Listeners.Files;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;

public class ListenerDownloadImageSuccess implements OnSuccessListener<Uri> {

    private Activity activity;
    private ImageButton imageButton;
    private String name;
    public ListenerDownloadImageSuccess(Activity activity, ImageButton imageButton, String name){
        this.activity = activity;
        this.imageButton = imageButton;
        this.name = name;
    }

    @Override
    public void onSuccess(Uri uri) {
        DownloadManager.Request request = new DownloadManager.Request(uri);
        String path = "/descargas";
        File directory = activity.getExternalFilesDir(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File file = new File(directory, name+".jpg");
        if (file.exists()) {
            file.delete();
        }
        request.setDestinationUri(Uri.fromFile(file));

        DownloadManager downloadManager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);

        BroadcastReceiver onComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bitmap bmp = BitmapFactory.decodeFile(activity.getExternalFilesDir(path) + "/" + name+".jpg");
                imageButton.setImageBitmap(bmp);
            }
        };
        activity.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }
}
