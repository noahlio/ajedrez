package com.example.ajedrez.Sonido;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import com.example.ajedrez.DAO.FirebaseStorageDAO;
import com.example.ajedrez.Enums.TypeFile;
import com.example.ajedrez.R;

public class SoundtrackManager extends Service {
    private MediaPlayer player;
    private IBinder mBinder = new LocalBinder();
    private int currentPosition;

    public class LocalBinder extends Binder {
        public SoundtrackManager getService() {
            return SoundtrackManager.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
            player = MediaPlayer.create(this, R.raw.menu_ost);
            player.setLooping(true);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        player.stop();
    }
}