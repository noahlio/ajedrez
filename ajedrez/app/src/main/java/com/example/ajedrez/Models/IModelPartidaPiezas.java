package com.example.ajedrez.Models;

public interface IModelPartidaPiezas {
    void moverPieza(int[] posicion);
    void comerPieza(int[] posicion);
}
