package com.example.ajedrez.Activities.Partidas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageButton;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.Activities.MainActivity;
import com.example.ajedrez.Activities.MyActivity;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.Listeners.ListenerActivity;
import com.example.ajedrez.Listeners.ListenerModal;
import com.example.ajedrez.Sonido.Services.SoundManager;
import com.example.ajedrez.Sonido.SoundtrackManager;
import com.example.ajedrez.R;
import com.example.ajedrez.VO.Resultados;

public class PartidaActivity extends MyActivity {

    protected Context context;
    protected Resources resources;
    protected SharedPreferences prefs;
    protected SharedPreferences.Editor prefsEditor;
    protected ImageButton imggoback;
    protected FirebaseDAO firebaseDAO;
    protected boolean online;
    protected String id;
    protected ListenerActivity listenerActivity;

    //======================================= FUNCIONES ONCREATE =======================================

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void config(){
        this.prefs = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        this.prefsEditor = prefs.edit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        listenerActivity.close();
    }

    //======================================= FUNCIONES ONLINE =======================================

    public void winOnline(){
        int victorias = prefs.getInt("Victories", 0);
        prefsEditor.putInt("Victories", victorias+1);
        prefsEditor.apply();
        ListenerActivity listenerActivity = new ListenerActivity(this, JugarActivity.class, getApplicationContext(), true);
        listenerActivity.close();
    }

    public void loseOnline(){
        int derrotas = prefs.getInt("Derrotes", 0);
        prefsEditor.putInt("Derrotes", derrotas+1);
        prefsEditor.apply();
    }

    public void readPartida(){
        if(!online) return;
        this.firebaseDAO.readPartida("partidas", id, this);
    }

    //======================================= FUNCIONES FIN PARTIDA =======================================

    public void finPartida(int resultado){
        SoundManager sndmanager = new SoundManager();
        if (resultado == Resultados.VICTORIA) {
            sndmanager.setSound(R.raw.player_win, this.context);
        } else {
            sndmanager.setSound(R.raw.player_lose, this.context);
        }
        sndmanager.start();
        ListenerModal listenerModal = new ListenerModal(this, R.layout.fin_partida, R.id.modal_partida_result,
                resultado == Resultados.VICTORIA ?
                        resources.getText(R.string.modal_partida_resultado_victoria).toString():
                        resultado == Resultados.DERROTA ?
                                resources.getText(R.string.modal_partida_resultado_derrota).toString():
                                resources.getText(R.string.modal_partida_resultado_empate).toString(),
                R.id.modal_menu_jugar, resources.getText(R.string.modal_return_menu_jugar).toString(),
                R.id.modal_menu_principal, resources.getText(R.string.modal_return_menu_principal).toString(),
                new int[] {}, new String[] {}, strings -> {
            Intent myIntent = new Intent(this.getApplicationContext(), JugarActivity.class);
            onDestroy(myIntent);
        }, bool -> {
            Intent myIntent = new Intent(this.getApplicationContext(), MainActivity.class);
            onDestroy(myIntent);
        }
        );
        setWinOrLose(resultado);
        listenerModal.showModal();
    }

    public void setWinOrLose(int resultado) {
        String accion = resultado==Resultados.VICTORIA?"Victories": resultado==Resultados.DERROTA? "Derrotes" : "Empats";
        prefsEditor.putInt(accion, prefs.getInt(accion, 0)+1);
        prefsEditor.apply();
    }
}
