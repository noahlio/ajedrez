package com.example.ajedrez.Listeners.Firebase;

import androidx.annotation.NonNull;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivityDamas;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.VO.Juegos;
import com.example.ajedrez.VO.Partida;
import com.example.ajedrez.VO.PartidaDamas;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.function.Consumer;

public class ListenerUpdatePartidaComplete implements OnCompleteListener<DocumentSnapshot> {

    private DocumentReference documentPartida;
    private Partida partida;
    private Consumer<Boolean> success;

    public ListenerUpdatePartidaComplete(DocumentReference documentPartida, Partida partida, Consumer<Boolean> success){
        this.documentPartida = documentPartida;
        this.partida = partida;
        this.success = success;
    }

    @Override
    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
        if(!task.isSuccessful()) return;
        DocumentSnapshot partidaBBDD = task.getResult();
        onUpdateDamas(partidaBBDD);
        success.accept(true);
    }

    private void onUpdateDamas(DocumentSnapshot partidaBBDD){
        if (!partida.getIdJuego().equals(Juegos.DAMAS)) return;

        PartidaDamas partidaDamas = (PartidaDamas) partida;

        //Si no existe poner los 2 nombres de los jugadores en blanco
        if (!partidaBBDD.exists()) {
            partidaDamas.setJ1("");
            partidaDamas.setJ2("");
            return;
        }
        documentPartida.update("j1", partidaDamas.getJ1());
        documentPartida.update("j2", partidaDamas.getJ2());
        documentPartida.update("turn", partidaDamas.getTurn());
        documentPartida.update("jugadas", partidaDamas.getJugadas());
    }
}
