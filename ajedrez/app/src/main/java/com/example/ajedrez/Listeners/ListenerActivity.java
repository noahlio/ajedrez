package com.example.ajedrez.Listeners;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.ajedrez.Activities.MyActivity;
import com.example.ajedrez.R;
import com.example.ajedrez.Sonido.Services.SoundManager;

public class ListenerActivity implements View.OnClickListener {

    private Class cls;
    private Context context;
    private MyActivity activity;
    private boolean destroy;
    private SoundManager sndmanager;

    public ListenerActivity(MyActivity activity, Class cls, Context context, boolean destroy){
        this.cls = cls;
        this.activity = activity;
        this.context = context;
        this.destroy = destroy;
        sndmanager = new SoundManager();
    }
    @Override
    public void onClick(View view) {
        this.sndmanager.setSound(R.raw.pressefect, this.context);
        this.sndmanager.start();
        close();
    }

    public void close(){

        Intent myIntent = new Intent(context, this.cls);
        if(destroy)
            activity.onDestroy(myIntent);
        else
            activity.startActivity(myIntent);
    }
}