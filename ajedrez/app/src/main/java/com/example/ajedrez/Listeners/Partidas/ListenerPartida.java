package com.example.ajedrez.Listeners.Partidas;

public class ListenerPartida {

    public int parseToOneDimension(int a, int b){
        return a*8 + b;
    }

    public int[] parseToTwoDimension(int a){
        return new int[] {a/8, a%8};
    }
}
