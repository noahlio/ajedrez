package com.example.ajedrez.Models;

public interface IModelPartidaTablero {
    public void rellenarTableroInicial();
    void control_pintarCasillas(int a, int b);
    void pintarCasillas(int a, int b);
    void pintarCasilla(int a, int b, boolean left);
    void pintarMovimiento(int a, int b);
    void pintarComida(int a, int b, boolean left);
    void seleccionarCasilla(int[] posicion);
    boolean in(int a, int b);
}
