package com.example.ajedrez.Listeners.Firebase;

import androidx.annotation.NonNull;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.VO.Juegos;
import com.example.ajedrez.VO.Partida;
import com.example.ajedrez.VO.PartidaDamas;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListenerFindPartidaToJoinComplete implements OnCompleteListener<QuerySnapshot> {

    private FirebaseFirestore db;
    private String collectionPath, id;
    private ListenerJoinPartidaEvent listenerJoinPartidaEvent;
    private ListenerRegistration listenerRegistration;

    public ListenerFindPartidaToJoinComplete(FirebaseFirestore db, String collectionPath, String id, ListenerJoinPartidaEvent listenerJoinPartidaEvent){
        this.db = db;
        this.collectionPath = collectionPath;
        this.id = id;
        this.listenerJoinPartidaEvent = listenerJoinPartidaEvent;
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        if (!task.isSuccessful()) return;
        for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
            if (!documentSnapshot.getId().equals(id)) continue;
            DocumentReference partida = db.collection(collectionPath).document(id);
            listenerJoinPartidaEvent.setListenerFindPartidaToJoinComplete(this);
            listenerRegistration = partida.addSnapshotListener(listenerJoinPartidaEvent);
        }
    }

    public String getIdPartida() {
        return this.id;
    }

    public void removeListenerRegistration(){
        this.listenerRegistration.remove();
    }
}
