package com.example.ajedrez.Models;

import android.util.Log;

import com.example.ajedrez.Exceptions.EmptyBoxException;
import com.example.ajedrez.Exceptions.OnlineRepeatException;
import com.example.ajedrez.Exceptions.WrongTurnException;
import com.example.ajedrez.Listeners.Partidas.ListenerPartidaDamas;
import com.example.ajedrez.VO.PartidaDamas;
import com.example.ajedrez.Piezas.Peon;
import com.example.ajedrez.VO.Resultados;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ModelPartidaDamas implements IModelPartida, IModelPartidaPiezas, IModelPartidaTablero{
    private ListenerPartidaDamas listener;
    private Long[][] tablero; //0 vacio, 1 J1, 2 J2, 3 J1 ha llegado al final, 4 J2 ha llegado al final
    /**
     * Lista de las jugadas que se han hecho.
     * Cada jugada es una lista de cuatro numeros
     * PosXInicial, PosYInicial, PosXFinal, PosYFinal
     */
    private Map<String, List<?>> jugadas;
    private boolean turn; //true J1, false J2
    private Peon fichaSeleccionada;
    int piezasJ1, piezasJ2;
    private boolean online, J1;

    public ModelPartidaDamas(boolean online, boolean J1){
        this.jugadas = new LinkedHashMap<String, List<?>>();
        this.online = online;
        this.J1 = J1;
    }

    //======================================= FUNCIONES LISTENER =======================================

    public void setListener(ListenerPartidaDamas listener){
        this.listener = listener;
    }

    //======================================= FUNCIONES INIT =======================================

    public void init(){
        this.piezasJ1 = 12;
        this.piezasJ2 = 12;
        this.tablero = new Long[8][8];
        this.turn = true;
        this.rellenarTableroInicial();
        this.listener.pintarTableroOnline(online, tablero);
    }

    public void rellenarTableroInicial(){
        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                if(x<=2 && y%2 != x%2) tablero[x][y] = Long.valueOf(2);
                else if(x>=5 && y%2 != x%2) tablero[x][y] = Long.valueOf(1);
                else tablero[x][y] = Long.valueOf(0);
            }
        }
    }

    //======================================= FUNCIONES ONLINE =======================================

    public void updatePartidaLocal(PartidaDamas partida) throws OnlineRepeatException {
        this.jugarJugadas(partida.getJugadas());
        this.listener.pintarTableroOnline(online, this.tablero);
        this.jugadas = new LinkedHashMap<String, List<?>>();
        this.turn = partida.getTurn();
        this.contarFichas();
        if(piezasJ1 == 0) listener.finPartida(J1? Resultados.DERROTA : Resultados.VICTORIA);
        if(piezasJ2 == 0) listener.finPartida(!J1? Resultados.DERROTA : Resultados.VICTORIA);
    }

    protected void jugarJugadas(Map<String, List<?>> jugadas) throws OnlineRepeatException {
        for(String nameJugada : jugadas.keySet()){
            List<?> jugada = jugadas.get(nameJugada);

            int posXInicial = Math.toIntExact((long) jugada.get(0));
            int posYInicial = Math.toIntExact((long) jugada.get(1));
            int posXFinal = Math.toIntExact((long) jugada.get(2));
            int posYFinal = Math.toIntExact((long) jugada.get(3));

            control_jugadaRepetida(posXInicial, posYInicial, posXFinal<0);

            Log.d("CASILLAS", "jugadaComer " + nameJugada + ": " + posXInicial + "," + posYInicial + " " + posXFinal + "," + posYFinal + " . " + turn);

            if(posXFinal<0) jugadaComer(posXInicial, posYInicial);
            else jugadaMover(posXInicial, posYInicial, posXFinal, posYFinal);
        }
    }

    private void jugadaComer(int a, int b){
        tablero[a][b] = Long.valueOf(0);
    }

    private void jugadaMover(int a, int b, int c, int d){
        tablero[c][d] = tablero[a][b];
        tablero[a][b] = Long.valueOf(0);
    }

    private void addJugada(int a, int b){
        if(!online) return;
        Long longA = Long.valueOf(a);
        Long longB = Long.valueOf(b);
        Long longNegative = Long.valueOf(-1);
        this.jugadas.put(String.valueOf(jugadas.size()), Arrays.asList(new Long[] {longA, longB, longNegative, longNegative}));
    }

    private void addJugada(int a, int b, int c, int d){
        if(!online) return;
        Long longA = Long.valueOf(a);
        Long longB = Long.valueOf(b);
        Long longC = Long.valueOf(c);
        Long longD = Long.valueOf(d);
        this.jugadas.put(String.valueOf(jugadas.size()),Arrays.asList(new Long[] {longA, longB, longC, longD}));
    }

    //======================================= FUNCIONES JUEGO =======================================

    public void seleccionarCasilla(int[] posicion) throws EmptyBoxException, WrongTurnException {
        int a = posicion[0];
        int b = posicion[1];

        if(fichaSeleccionada != null){
            if(fichaSeleccionada.containsPosicionesComida(posicion)){ //si se puede comer
                moverPieza(posicion);
                comerPieza(posicion);
            }
            else if(fichaSeleccionada.isComerObligada()){ //si solo puede comer
                pintarSiguienteComida();
                return;
            }
            else if(fichaSeleccionada.containsPosicionesMovimiento(posicion)){ //si se puede mover
                moverPieza(posicion);
                transformarPieza();
                cambioTurno();
            }
            else{
                transformarPieza();
            }
        }
        else{
            this.control_pintarCasillas(a, b);
            this.fichaSeleccionada = new Peon(a, b, tablero[a][b]);
            this.pintarCasillas(a, b);
        }
    }

    private void transformarPieza(){
        Long pieza = fichaSeleccionada.getColor();
        int posX = fichaSeleccionada.getPosicion()[0];
        int posY = fichaSeleccionada.getPosicion()[1];
        if((pieza==1) && posX==0) this.tablero[posX][posY] = Long.valueOf(3);
        if((pieza==2) && posX==7) this.tablero[posX][posY] = Long.valueOf(4);
        if((pieza==3) && posX==7) this.tablero[posX][posY] = Long.valueOf(1);
        if((pieza==4) && posX==0) this.tablero[posX][posY] = Long.valueOf(2);
        fichaSeleccionada = null;
    }

    public void moverPieza(int[] posicion){
        int a = posicion[0];
        int b = posicion[1];
        this.tablero[fichaSeleccionada.getPosX()][fichaSeleccionada.getPosY()] = this.tablero[a][b];
        this.tablero[a][b] = fichaSeleccionada.getColor();

        this.addJugada(fichaSeleccionada.getPosX(),fichaSeleccionada.getPosY(), a, b);

        this.listener.moverPieza(posicion, fichaSeleccionada.getPosicion());
    }

    public void comerPieza(int[] posicion){
        //eliminar ficha enemiga
        int[] posicionFichaComida = this.getPosicionFichaComida(fichaSeleccionada.getPosicion(), posicion);
        this.tablero[posicionFichaComida[0]][posicionFichaComida[1]] = Long.valueOf(0);

        this.addJugada(posicionFichaComida[0],posicionFichaComida[1]);

        this.listener.comerPieza(posicionFichaComida, turn);
        fichaSeleccionada.resetPosiciones();

        //mover ficha
        this.fichaSeleccionada.setPosX(posicion[0]);
        this.fichaSeleccionada.setPosY(posicion[1]);

        pintarSiguienteComida();

        if(fichaSeleccionada.getPosicionesComida().size()>0){ // si puede comer
            fichaSeleccionada.setComerObligada(true);
            return;
        }
        transformarPieza();
        cambioTurno();
    }

    public void perderPieza(boolean turno){
        if(turno) piezasJ1--;
        else piezasJ2--;
        if(piezasJ1 == 0) listener.finPartida(J1? Resultados.DERROTA : Resultados.VICTORIA);
        if(piezasJ2 == 0) listener.finPartida(!J1? Resultados.DERROTA : Resultados.VICTORIA);
    }

    private void cambioTurno(){
        if(online)listener.setPartidaOnline(jugadas, turn);
        this.turn = !this.turn;
        this.listener.swapTimer(turn);
    }

    //======================================= FUNCIONES CONTROL =======================================

    private void control_jugadaRepetida(int a, int b, boolean comida) throws OnlineRepeatException {
        if(tablero[a][b] == 0){
            throw new OnlineRepeatException("La casilla [" + a + "," + b + "] esta vacia");
        }
        if((tablero[a][b] == 1 || tablero[a][b] == 3) && J1 && !comida){
            throw new OnlineRepeatException("Jugada en la casilla [" + a + "," + b + "] repetida");
        }
        if((tablero[a][b] == 2 || tablero[a][b] == 4) && !J1 && !comida){
            throw new OnlineRepeatException("Jugada en la casilla [" + a + "," + b + "] repetida");
        }
    }

    public void control_pintarCasillas(int a, int b) throws EmptyBoxException, WrongTurnException {
        Long pieza = this.tablero[a][b];
        if(online && turn != J1){
            throw new WrongTurnException("No es tu turno online " + turn);
        }
        if(pieza == 0){
            throw new EmptyBoxException("No hay pieza");
        }
        if(pieza%2==1 != turn){
            Log.d("tag", "control_pintarCasillas: " + turn);
            throw new WrongTurnException("No es tu turno " + (turn?"J1":"J2"));
        }
    }

    //======================================= FUNCIONES PINTAR =======================================

    public void pintarCasillas(int a, int b){
        Long pieza = this.tablero[a][b];
        int x = a - (pieza == 1 || pieza == 4? 1:-1); //piezas que van hacia arriba
        pintarCasilla(x, b-1, true);
        pintarCasilla(x, b+1, false);
    }

    public void pintarCasilla(int a, int b, boolean left){
        pintarMovimiento(a, b);
        pintarComida(a, b, left);
    }

    public void pintarMovimiento(int a, int b){
        if(!in(a,b)) return;

        Long pieza = this.tablero[a][b]; //pieza en posicion a pintar
        if(pieza != 0) return; //si hay pieza

        this.fichaSeleccionada.addPosicionMovimiento(a, b);
        this.listener.pintarCasilla(a, b);
    }

    public void pintarComida(int a, int b, boolean left){
        if(!in(a,b)) return;

        Long pieza = fichaSeleccionada.getColor();
        Long piezaEnemiga = this.tablero[a][b]; //pieza en posicion a pintar
        if(piezaEnemiga == 0) return; //si no hay pieza
        if(turn != (piezaEnemiga%2==0)) return; //si no es pieza enemiga

        int a2 = a - (pieza == 1 || pieza == 4?1:-1); //si la pieza se mueve hacia abajo
        int b2 = b - (left?1:-1);
        if(!in(a2,b2)) return;

        Long piezaDetrasEnemiga = this.tablero[a2][b2]; //pieza en posicion despues a la enemiga
        if(piezaDetrasEnemiga != 0) return; //si hay pieza

        this.fichaSeleccionada.addPosicionComida(a2, b2);
        this.listener.pintarCasilla(a2, b2);
    }

    private void pintarSiguienteComida(){
        int a = fichaSeleccionada.getPosicion()[0];
        int b = fichaSeleccionada.getPosicion()[1];
        Long pieza = this.tablero[a][b];
        int x = a - (pieza == 1 || pieza == 4? 1:-1); //piezas que van hacia arriba
        pintarComida(x, b-1, true);
        pintarComida(x, b+1, false);
    }

    //======================================= FUNCIONES CALCULO =======================================

    private int[] getPosicionFichaComida(int[] posicionActual, int[] posicionAnterior){
        return new int[] {(posicionActual[0] + posicionAnterior[0])/2, (posicionActual[1] + posicionAnterior[1])/2};
    }

    public boolean in(int a, int b){
        return a>=0 && b>=0 && a<8 && b<8;
    }

    private void contarFichas(){
        int piezasJ1 = 0;
        int piezasJ2 = 0;
        for(int x = 0;x<8; x++){
            for(int y = 0; y<8 ; y++){
                if(tablero[x][y] == 0){
                    continue;
                }
                if(tablero[x][y] % 2 == 0){
                    piezasJ2++;
                }
                else{
                    piezasJ1++;
                }
            }
        }
        this.piezasJ1 = piezasJ1;
        this.piezasJ2 = piezasJ2;
    }
}
