package com.example.ajedrez.Listeners.Partidas;

public interface IListenerPartidaPiezas {
    void moverPieza(int[] c1, int[] c2);
    void comerPieza(int[] posicion, boolean turn);
}
