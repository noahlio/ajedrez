package com.example.ajedrez.Listeners.Toast;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;

public class ListenerSuccessToast implements OnSuccessListener {

    private Context context;
    private String successMessage;

    public ListenerSuccessToast(Context context, String successMessage){
        this.context = context;
        this.successMessage = successMessage;
    }

    @Override
    public void onSuccess(Object o) {
        Toast.makeText(context, successMessage, Toast.LENGTH_SHORT).show();
    }
}
