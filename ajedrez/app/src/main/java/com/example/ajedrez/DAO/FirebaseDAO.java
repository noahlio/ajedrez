package com.example.ajedrez.DAO;

import android.app.ProgressDialog;
import android.util.Log;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.Activities.MyActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivityDamas;
import com.example.ajedrez.Listeners.Firebase.ListenerFindPartidaComplete;
import com.example.ajedrez.Listeners.Firebase.ListenerFindPartidaToJoinComplete;
import com.example.ajedrez.Listeners.Firebase.ListenerInsertPartidaSuccess;
import com.example.ajedrez.Listeners.Firebase.ListenerJoinPartidaEvent;
import com.example.ajedrez.Listeners.Firebase.ListenerNoJoinPartidaCancel;
import com.example.ajedrez.Listeners.Firebase.ListenerReadPartidaEvent;
import com.example.ajedrez.Listeners.Firebase.ListenerReadPartidaComplete;
import com.example.ajedrez.Listeners.Firebase.ListenerReadPartidaToJoinComplete;
import com.example.ajedrez.Listeners.Firebase.ListenerUpdatePartidaComplete;
import com.example.ajedrez.VO.Partida;
import com.example.ajedrez.VO.PartidaDamas;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Consumer;

public class FirebaseDAO {

    private FirebaseFirestore db;
    private Long idJuego;
    private ListenerReadPartidaComplete listenerReadPartidaComplete;

    public FirebaseDAO(Long idJuego){
        this.db = FirebaseFirestore.getInstance();
        this.idJuego = idJuego;
    }

    //====================================== FUNCIONES INSERT ======================================

    public void insertInCollection(Partida partida, String collectionPath, JugarActivity jugarActivity) throws InterruptedException {
        Map<String, Object> collection = partida.toMap();
        Task<DocumentReference> insert = this.db.collection(collectionPath).add(collection);
        insert.addOnSuccessListener(new ListenerInsertPartidaSuccess(partida, collectionPath, jugarActivity));
    }

    //====================================== FUNCIONES SELECT ======================================

    public void readPartida(String collectionPath, String id, PartidaActivity partidaActivity){
        Log.d("FirebaseDAO", "readPartida: leer partida " + id);
        Task<QuerySnapshot> selectPartida = db.collection(collectionPath).get();
        ListenerReadPartidaEvent listenerReadPartidaEvent = new ListenerReadPartidaEvent(this, partidaActivity);
        listenerReadPartidaComplete = new ListenerReadPartidaComplete(db, collectionPath, id, listenerReadPartidaEvent);
        selectPartida.addOnCompleteListener(listenerReadPartidaComplete);
    }

    public void searchGameToJoin(String collectionPath, JugarActivity jugarActivity) {
        Task<QuerySnapshot> search = db.collection(collectionPath).get();
        ListenerReadPartidaToJoinComplete listenerReadPartidaToJoinComplete = new ListenerReadPartidaToJoinComplete(this, jugarActivity);
        search.addOnCompleteListener(listenerReadPartidaToJoinComplete);
    }

    private Partida getPartidaGeneral(DocumentSnapshot document){
        Partida partida = new Partida();
        partida.setId(document.getId());
        if(document.getData() == null) return partida;
        partida.setIdJuego((Long) document.getData().get("idJuego"));
        partida.setJugadas((Map<String, List<?>>) document.getData().get("jugadas"));
        return partida;
    }

    public PartidaDamas getPartidaDamas(DocumentSnapshot document){
        PartidaDamas partida = new PartidaDamas(this.getPartidaGeneral(document));
        if (partida.getIdJuego() == null) {
            partida.setJ2("");
            partida.setJ1("");
            return partida;
        }
        partida.setTurn((Boolean) document.getData().get("turn"));
        partida.setJ1((String) document.getData().get("j1"));
        partida.setJ2((String) document.getData().get("j2"));
        return partida;
    }

    public void FindWaitingMatch(String collectionPath, String nombre) {
        Task<QuerySnapshot> find = db.collection(collectionPath).get();
        ListenerFindPartidaComplete listenerFindPartidaComplete = new ListenerFindPartidaComplete(db, nombre, idJuego);
        find.addOnCompleteListener(listenerFindPartidaComplete);
    }

    //====================================== FUNCIONES UPDATE ======================================

    public void updateCollection(String collectionPath, Partida partida, Consumer<Boolean> success) {
        DocumentReference documentPartida = db.collection(collectionPath).document(partida.getId());
        //Hacer listener de si sigue existiendo el documento en la BBDD
        ListenerUpdatePartidaComplete listenerUpdatePartidaComplete = new ListenerUpdatePartidaComplete(documentPartida, partida, success);
        Task<DocumentSnapshot> snapshotPartida = documentPartida.get();
        snapshotPartida.addOnCompleteListener(listenerUpdatePartidaComplete);
    }

    //====================================== FUNCIONES DELETE ======================================

    public void RemoveMatch(String idPartida) {
        db.collection("partidas").document(idPartida).delete();
        if(listenerReadPartidaComplete == null) return;
        listenerReadPartidaComplete.removeListenerRegistration();
    }

    //====================================== FUNCIONES MATCH =======================================

    public void FindCollection(String collectionPath, String id, ProgressDialog waitingmsg, JugarActivity jugarActivity) {
        Task<QuerySnapshot> find = db.collection(collectionPath).get();

        ListenerNoJoinPartidaCancel listenerNoJoinPartidaCancel = new ListenerNoJoinPartidaCancel(bool -> {RemoveMatch(id);});
        waitingmsg.setOnCancelListener(listenerNoJoinPartidaCancel);

        ListenerJoinPartidaEvent listenerJoinPartidaEvent = new ListenerJoinPartidaEvent(waitingmsg, jugarActivity, idJuego, listenerNoJoinPartidaCancel);
        ListenerFindPartidaToJoinComplete listenerFindPartidaToJoinComplete = new ListenerFindPartidaToJoinComplete(db, collectionPath, id, listenerJoinPartidaEvent);

        find.addOnCompleteListener(listenerFindPartidaToJoinComplete);
    }

    //================================== FUNCIONES GETTER/SETTER ===================================

    public Long getIdJuego(){
        return this.idJuego;
    }
}