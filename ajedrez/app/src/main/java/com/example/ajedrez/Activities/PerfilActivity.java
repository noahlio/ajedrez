package com.example.ajedrez.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajedrez.DAO.FirebaseStorageDAO;
import com.example.ajedrez.Enums.TypeFile;
import com.example.ajedrez.Idioma.LocaleHelper;
import com.example.ajedrez.Listeners.Files.ListenerUploadFile;
import com.example.ajedrez.Listeners.ListenerActivity;
import com.example.ajedrez.Listeners.ListenerModal;
import com.example.ajedrez.R;

import java.io.File;
import java.util.function.Consumer;

public class PerfilActivity extends MyActivity {

    private ImageButton imggoback, btnEditarNombre, btnImagenInicio;
    private Context context;
    private Resources resources;
    private TextView stats, victorias, derrotas, name, victoriascount, derrotascount;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editorpreferencies;
    private FirebaseStorageDAO firebaseStorageDAO;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        context = LocaleHelper.setLocale(PerfilActivity.this,LocaleHelper.getLanguage(PerfilActivity.this));
        resources =context.getResources();
        this.prefs = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        this.editorpreferencies = prefs.edit();
        this.firebaseStorageDAO = new FirebaseStorageDAO(this);

        initElements();
        onLanguageChangeOrLoad();

    }
    private void initElements(){

        this.imggoback = (ImageButton) findViewById(R.id.btnPerfilToMenu);
        this.imggoback.setImageResource(R.drawable.back_arrow);
        this.imggoback.setOnClickListener(new ListenerActivity(this, MainActivity.class, getApplicationContext(), true));

        this.btnImagenInicio = (ImageButton) findViewById(R.id.btnImagenInicio);
        this.btnImagenInicio.setOnClickListener(new ListenerUploadFile(this));
        firebaseStorageDAO.downloadFile(TypeFile.fotoPerfil, this.btnImagenInicio);

        this.name = findViewById(R.id.player_name);
        this.name.setText(prefs.getString("NOM", "Guest"));

        this.stats = findViewById(R.id.lbl_historial);

        this.victorias = findViewById(R.id.player_wins);
        this.derrotas = findViewById(R.id.player_loses);

        this.victoriascount = findViewById(R.id.player_wins_count);
        int numvictories = prefs.getInt("Victories", -1);
        String victoriess = String.valueOf(numvictories);
        this.victoriascount.setText(victoriess);


        this.derrotascount = findViewById(R.id.player_lose_count);
        int numderrotes = prefs.getInt("Derrotes", -1);
        String derrotess = String.valueOf(numderrotes);
        this.derrotascount.setText(derrotess);

        this.btnEditarNombre = findViewById(R.id.btnEditarNombre);
        this.btnEditarNombre.setOnClickListener(new ListenerModal(this, R.layout.edit_name, R.id.modal_editname,
                resources.getText(R.string.modal_change_name).toString(),
                R.id.modal_edit_name_accept, resources.getText(R.string.modal_accept).toString(),
                R.id.modal_edit_name_cancel, resources.getText(R.string.modal_decline).toString(),
                new int[] {R.id.modal_edit_name_name}, new String[] {"Guest"},
                strings -> {
                    String nom = strings.get(0);
                    if(nom.length()==0) return;
                    editorpreferencies.putString("NOM", nom);
                    editorpreferencies.apply();
                    this.name.setText(strings.get(0));
                },
                bool -> {}
        ));
    }
    public void onLanguageChangeOrLoad() {
        this.stats.setText(resources.getText(R.string.player_stats));
        this.victorias.setText(resources.getText(R.string.player_wins));
        this.derrotas.setText(resources.getText(R.string.player_loses));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == 100 && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();

            this.firebaseStorageDAO.uploadFile(uri, "fotoPerfil",
                    "Foto de perfil actualizada correctamente",
                    "No se ha podido subir la foto de perfil");

            this.btnImagenInicio.setImageURI(uri);
        }
    }
}