package com.example.ajedrez.Listeners.Firebase;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListenerReadPartidaComplete implements OnCompleteListener<QuerySnapshot> {

    private FirebaseFirestore db;
    private String collectionPath;
    private String id;
    private ListenerReadPartidaEvent listenerReadPartidaEvent;
    private ListenerRegistration listenerRegistration;

    public ListenerReadPartidaComplete(FirebaseFirestore db, String collectionPath, String id, ListenerReadPartidaEvent listenerReadPartidaEvent){
        this.db = db;
        this.collectionPath = collectionPath;
        this.id = id;
        this.listenerReadPartidaEvent = listenerReadPartidaEvent;
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        for (QueryDocumentSnapshot document : task.getResult()) {
            if(!document.getId().equals(id)) continue;
            DocumentReference documentReference = db.collection(collectionPath).document(id);
            this.listenerRegistration = documentReference.addSnapshotListener(listenerReadPartidaEvent);
            break;
        }
    }

    public void removeListenerRegistration(){
        this.listenerRegistration.remove();
    }
}
