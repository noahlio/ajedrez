package com.example.ajedrez.Listeners.Partidas;

public interface IListenerPartidaTablero {
    void pintarCasilla(int a, int b);
    void pintarTableroOnline(boolean online, Long[][] tablero);
}
