package com.example.ajedrez.Adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import com.example.ajedrez.Enums.TypeAdapter;
import com.example.ajedrez.Listeners.Partidas.ListenerPartidaDamas;
import com.example.ajedrez.Listeners.ListenerPieza;
import com.example.ajedrez.R;

import java.util.ArrayList;

public class AdaptadorCasillas extends ArrayAdapter {
    private Activity context;
    private ArrayList<Integer> content;
    private int resource;
    private TypeAdapter type;
    private ListenerPartidaDamas listenerPartidaDamas;

    public AdaptadorCasillas(Activity context, ArrayList<Integer> content, int resource, TypeAdapter type, ListenerPartidaDamas listenerPartidaDamas) {
        super(context, R.layout.item_casilla, content);
        this.context = (Activity) context;
        this.content = content;
        this.resource = resource;
        this.type = type;
        this.listenerPartidaDamas = listenerPartidaDamas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(resource, null);
        switch (this.type){
            case Piezas:
                ImageButton casilla = (ImageButton) item.findViewById(R.id.item_casilla);
                casilla.setImageResource(content.get(position));
                ListenerPieza onClickListener = new ListenerPieza(listenerPartidaDamas, position);
                casilla.setOnClickListener(onClickListener.getOnClickListener());
            case Fondo:
                item.setBackgroundColor(content.get(position));
                if (content.get(position) % 2 == 0) {
                    item.setBackgroundColor(content.get(position));
                }
                else if (content.get(position) % 2 != 0) {
                    item.setBackgroundColor(content.get(position));
                }
        }
        return (item);
    }
}