package com.example.ajedrez.Adaptadores;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ajedrez.R;
import com.example.ajedrez.VO.Juegos;

import java.util.List;
import java.util.Locale;

public class AdaptadorListaJuegos extends ArrayAdapter<String> {
    Context context;
    List<String> juegos;

    public AdaptadorListaJuegos(@NonNull Context context, List<String> juegos) {
        super(context, R.layout.spinner_game_selected, juegos);
        this.context = context;
        this.juegos = juegos;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getFilasVista(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getFilasVista(position, convertView, parent);
    }

    //Devuelve como la "fila" de dentro del adaptador por cada elemento de la lista.
    public View getFilasVista(int pos, View vista, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.spinner_game_selected, parent, false);

        ImageView imgjuego = row.findViewById(R.id.selector_juego_img);
        //Setear el texto
        //Setear la imagen
        Resources res = context.getResources();
        //El nombre del icono del juego tiene que ser la misma que el nombre de la opcion
        String nomimg =  juegos.get(pos).toLowerCase();
        int resId = res.getIdentifier(nomimg, "drawable", context.getPackageName());
        Drawable drawable = res.getDrawable(resId);
        imgjuego.setImageDrawable(drawable);

        return row;
    }
}
