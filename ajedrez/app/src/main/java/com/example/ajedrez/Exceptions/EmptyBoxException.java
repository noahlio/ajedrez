package com.example.ajedrez.Exceptions;

import java.io.FileNotFoundException;

public class EmptyBoxException extends IllegalArgumentException{
    public EmptyBoxException() {
        super("No hay pieza");
    }

    public EmptyBoxException(String s) {
        super(s);
    }
}
