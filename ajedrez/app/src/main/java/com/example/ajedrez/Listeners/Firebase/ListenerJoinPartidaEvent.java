package com.example.ajedrez.Listeners.Firebase;

import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivityDamas;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.VO.Juegos;
import com.example.ajedrez.VO.PartidaDamas;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class ListenerJoinPartidaEvent implements EventListener<DocumentSnapshot> {

    private ProgressDialog waitingMsg;
    private JugarActivity jugarActivity;
    private Long idJuego;
    private ListenerFindPartidaToJoinComplete listenerFindPartidaToJoinComplete;
    private ListenerNoJoinPartidaCancel listenerNoJoinPartidaCancel;

    public ListenerJoinPartidaEvent(ProgressDialog waitingMsg, JugarActivity jugarActivity, Long idJuego, ListenerNoJoinPartidaCancel listenerNoJoinPartidaCancel){
        this.waitingMsg = waitingMsg;
        this.jugarActivity = jugarActivity;
        this.idJuego = idJuego;
        this.listenerNoJoinPartidaCancel = listenerNoJoinPartidaCancel;
    }

    @Override
    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
        startDamas(value);
    }

    private void startDamas(@Nullable DocumentSnapshot value){
        if(!idJuego.equals(Juegos.DAMAS)) return;
        if (value == null || !value.exists()) return;
        if (value.get("j2").equals("")) return;

        if(!listenerNoJoinPartidaCancel.isCancelled()){
            waitingMsg.dismiss();
            Intent intent = new Intent(jugarActivity, PartidaActivityDamas.class);
            intent.putExtra("id", listenerFindPartidaToJoinComplete.getIdPartida());
            intent.putExtra("j1", (String)value.get("j1"));
            intent.putExtra("j2", (String)value.get("j2"));
            intent.putExtra("jugador", "j1");
            jugarActivity.startActivity(intent);
        }
        listenerFindPartidaToJoinComplete.removeListenerRegistration();
    }

    public void setListenerFindPartidaToJoinComplete(ListenerFindPartidaToJoinComplete listenerFindPartidaToJoinComplete) {
        this.listenerFindPartidaToJoinComplete = listenerFindPartidaToJoinComplete;
    }
}
