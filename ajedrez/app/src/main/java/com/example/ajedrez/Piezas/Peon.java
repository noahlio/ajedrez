package com.example.ajedrez.Piezas;

import android.util.Log;

import java.util.ArrayList;

public class Peon{
    private int a;
    private int b;
    private Long color;
    private ArrayList<int[]> posicionesMovimiento;
    private ArrayList<int[]> posicionesComida;
    private boolean comerObligada;
    public Peon(int a, int b, Long color){
        this.a = a;
        this.b = b;
        this.color = color;
        this.resetPosiciones();
        this.comerObligada = false;
    }
    public void resetPosiciones(){
        this.posicionesMovimiento = new ArrayList<int[]>();
        this.posicionesComida = new ArrayList<int[]>();
    }
    public void addPosicionMovimiento(int a, int b){
        this.posicionesMovimiento.add(new int[] {a,b});
    }
    public void addPosicionComida(int a, int b){
        this.posicionesComida.add(new int[] {a,b});
    }
    public boolean containsPosicionesMovimiento(int[] pos){
        return this.containsPos(this.posicionesMovimiento, pos);
    }
    public boolean containsPosicionesComida(int[] pos){
        return this.containsPos(this.posicionesComida, pos);
    }
    public boolean containsPos(ArrayList<int[]> posiciones, int[] pos){
        if(pos.length != 2) return false;
        for(int[] posicion : posiciones){
            if(posicion[0] == pos[0] && posicion[1] == pos[1]){
                return true;
            }
        }
        return false;
    }
    public void setColor(Long color){
        this.color = color;
    }
    public Long getColor(){
        return this.color;
    }
    public int getPosX(){
        return this.a;
    }
    public void setPosX(int a){
        this.a = a;
    }
    public int getPosY(){
        return this.b;
    }
    public void setPosY(int b){
        this.b = b;
    }
    public boolean isComerObligada(){
        return this.comerObligada;
    }
    public void setComerObligada(boolean comerObligada){
        this.comerObligada = comerObligada;
    }
    public int[] getPosicion(){
        return new int[] {this.a, this.b};
    }
    public ArrayList<int[]> getPosicionesComida(){
        return this.posicionesComida;
    }
}