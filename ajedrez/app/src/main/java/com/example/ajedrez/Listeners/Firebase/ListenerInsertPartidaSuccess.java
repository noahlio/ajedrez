package com.example.ajedrez.Listeners.Firebase;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.VO.Partida;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;

public class ListenerInsertPartidaSuccess implements OnSuccessListener<DocumentReference> {

    private Partida partida;
    private String collectionPath;
    private JugarActivity jugarActivity;

    public ListenerInsertPartidaSuccess(Partida partida, String collectionPath, JugarActivity jugarActivity){
        this.partida = partida;
        this.collectionPath = collectionPath;
        this.jugarActivity = jugarActivity;
    }
    @Override
    public void onSuccess(DocumentReference documentReference) {
        jugarActivity.setId(documentReference.getId());
        partida.setId(documentReference.getId());
        jugarActivity.unirSala(partida);
    }
}
