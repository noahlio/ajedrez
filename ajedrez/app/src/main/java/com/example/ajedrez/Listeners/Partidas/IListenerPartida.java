package com.example.ajedrez.Listeners.Partidas;

import com.example.ajedrez.Exceptions.OnlineRepeatException;
import com.example.ajedrez.VO.PartidaDamas;

public interface IListenerPartida {
    void init();
    void seleccionar(int posicion);
    void getPartidaOnline(PartidaDamas partida) throws OnlineRepeatException;
    void finPartida(int resultado);
}
