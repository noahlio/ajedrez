package com.example.ajedrez.Activities.Partidas;

import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.Adaptadores.AdaptadorCasillas;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.Enums.TypeAdapter;
import com.example.ajedrez.Exceptions.OnlineRepeatException;
import com.example.ajedrez.Idioma.LocaleHelper;
import com.example.ajedrez.Listeners.ListenerAbandonarPartida;
import com.example.ajedrez.Listeners.ListenerActivity;
import com.example.ajedrez.Listeners.Partidas.ListenerPartidaDamas;
import com.example.ajedrez.Models.ModelPartidaDamas;
import com.example.ajedrez.Sonido.Services.SoundManager;
import com.example.ajedrez.VO.Juegos;
import com.example.ajedrez.VO.PartidaDamas;
import com.example.ajedrez.R;
import com.example.ajedrez.VO.Resultados;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PartidaActivityDamas extends PartidaActivity implements IPartidaActivity, IPartidaActivity2Players {

    private ArrayList<Integer> fondos;
    private ArrayList<Integer> piezas;
    private ListenerPartidaDamas listener;
    private TextView nombreJugador, nombreJugador2, timerJ2, timerJ1;
    private boolean J1;
    private String nomJ1, nomJ2;
    private PartidaDamas partidaDamas;
    private long segundostimerJ1, segundostimerJ2;
    private CountDownTimer countDownTimerJ1, countDownTimerJ2;
    private SoundManager soundManager;

    //======================================= FUNCIONES ONCREATE =======================================

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partida);
        config();
        fondoDegradado();
        readPartida();
        createGame();
        initElements();
        listener.init();
    }

    protected void config(){
        super.config();
        context = LocaleHelper.setLocale(PartidaActivityDamas.this,LocaleHelper.getLanguage(PartidaActivityDamas.this));
        resources =context.getResources();
        onLanguageChangeOrLoad();
        getIntentData();
        this.firebaseDAO = new FirebaseDAO(prefs.getLong("Juego", Juegos.DAMAS));
    }

    public void onLanguageChangeOrLoad() {

    }

    private void getIntentData(){
        this.id = getIntent().getStringExtra("id");
        if(id == null){
            this.online = false;
            this.J1 = true;
            this.nomJ1 = prefs.getString("NOM", "Guest");
            this.nomJ2 = "Guest2";
            return;
        }
        this.online = true;
        this.nomJ1 = getIntent().getStringExtra("j1");
        this.nomJ2 = getIntent().getStringExtra("j2");
        if(getIntent().getStringExtra("jugador").equals("j1")) J1 = true;
        else J1 = false;
    }

    private void fondoDegradado(){
        ConstraintLayout constraintLayout = findViewById(R.id.bg);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(500);
        animationDrawable.setExitFadeDuration(2500);
        animationDrawable.start();
    }

    public void readPartida(){
        if(!online) return;
        this.firebaseDAO.readPartida("partidas", id, this);
    }

    private void createGame(){
        ModelPartidaDamas model = new ModelPartidaDamas(online, J1);
        ListenerPartidaDamas listener = new ListenerPartidaDamas(model, this);
        model.setListener(listener);
        this.setListener(listener);
    }

    private void initElements(){
        this.soundManager = new SoundManager();
        fondos = this.generarCasillas();
        piezas = this.generarPiezas();

        updateGridViewCasillas();
        updateGridViewPiezas();

        this.nombreJugador = (TextView) findViewById(R.id.player1_name);
        this.nombreJugador.setText(this.nomJ1);

        this.nombreJugador2 = (TextView) findViewById(R.id.player2_name);
        this.nombreJugador2.setText(this.nomJ2);

        this.imggoback = (ImageButton) findViewById(R.id.img_goback);
        this.imggoback.setImageResource(R.drawable.back_arrow);

        this.listenerActivity = online? new ListenerAbandonarPartida(this, JugarActivity.class, getApplicationContext(), true):
                new ListenerActivity(this, JugarActivity.class, getApplicationContext(), true);

        this.timerJ1 = findViewById(R.id.timerJ1);
        this.timerJ2 = findViewById(R.id.TimerJ2);
        this.timerJ1.setText("0:00");
        this.timerJ2.setText("0:00");
        this.imggoback.setOnClickListener(listenerActivity);
        if (!this.online) {
            this.segundostimerJ1 = 10000;
            this.segundostimerJ2 = 10000;
            updateTimer(true);
            updateTimer(false);
            this.countDownTimerJ1 = new CountDownTimer(segundostimerJ1, 1000) {
                @Override
                public void onTick(long l) {
                    segundostimerJ1 = l;
                    updateTimer(true);
                }
                @Override
                public void onFinish() {
                    listener.finPartida(Resultados.DERROTA);
                    stopTimer(true);
                    stopTimer(false);
                }
            }.start();
            this.countDownTimerJ2 = new CountDownTimer(segundostimerJ2, 1000) {
                @Override
                public void onTick(long l) {
                    segundostimerJ2 = l;
                    updateTimer(false);
                }
                @Override
                public void onFinish() {
                    listener.finPartida(Resultados.DERROTA);
                    stopTimer(true);
                    stopTimer(false);
                }
            };
        }
    }

    public void updateTimer(boolean turno) {
        //prefsEditor.putLong("timerJ1", this.segundostimerJ1);
        //prefsEditor.putLong("timerJ2", this.segundostimerJ2);
        int minutos = 0;
        int segundos = 0;
        if (turno) {
            minutos = (int) this.segundostimerJ1 / 60000;
            segundos = (int) this.segundostimerJ1 % 60000 / 1000;
        }
        else {
            minutos = (int) this.segundostimerJ2 / 60000;
            segundos = (int) this.segundostimerJ2 % 60000 / 1000;
        }

        String tiempo = "" + minutos;
        tiempo+= ":";
        if (segundos < 10) tiempo+= "0";
        tiempo += segundos;

        if (turno) {
            this.timerJ1.setText(tiempo);
        } else {
            this.timerJ2.setText(tiempo);
        }
    }

    //Guardar en preferencias los milisegundos y cuando se le de a start se recuperen y de ahi vaya bajando el timer
    public void startTimer(boolean turn) {
        if (online) return;
        //prefs.getLong("timerJ1", this.segundostimerJ1);
        //prefs.getLong("timerJ2", this.segundostimerJ2);
        if (turn) this.countDownTimerJ1.start();
        if (!turn) this.countDownTimerJ2.start();
    }
    public void stopTimer(boolean turn) {
        if (online) return;
        //prefsEditor.putLong("timerJ1", this.segundostimerJ1);
        //prefsEditor.putLong("timerJ2", this.segundostimerJ2);
        if (turn) this.countDownTimerJ1.cancel();
        if (!turn) this.countDownTimerJ2.cancel();
    }

    //======================================= FUNCIONES ONLINE =======================================

    public void getPartidaOnline(PartidaDamas partidaDamas){
        this.partidaDamas = partidaDamas;
        winOnline();
        if (partidaDamas.gameEmpty()) return;
        try{
            this.listener.getPartidaOnline(partidaDamas);
        } catch (OnlineRepeatException e) {
            Log.e("ONLINE", "updatePartidaLocal: " + e.getMessage());
        }
    }

    public void setPartidaOnline(Map<String, List<?>> jugadas, boolean turn){
        this.partidaDamas.setJugadas(jugadas);
        this.partidaDamas.setTurn(turn);

        //Al actualizar la coleccion buscará si existe el documento
        this.firebaseDAO.updateCollection("partidas", partidaDamas, bool -> {winOnline();});
    }

    public void winOnline(){
        if (!partidaDamas.gameEmpty()) return;
        super.winOnline();
        soundManager.setSound(R.raw.player_win, this.context);
        soundManager.start();
    }

    public void loseOnline(){
        firebaseDAO.RemoveMatch(partidaDamas.getId());
        super.loseOnline();
        soundManager.setSound(R.raw.player_lose, this.context);
        soundManager.start();
    }

    //============================== FUNCIONES GENERAR CASILLAS Y PIEZAS ==============================

    private void updateGridViewCasillas(){
        AdaptadorCasillas adaptadorFondo = new AdaptadorCasillas(this, fondos, R.layout.item_fondo, TypeAdapter.Fondo, listener);
        GridView fondo = (GridView) findViewById(R.id.fondo);
        fondo.setAdapter(adaptadorFondo);
    }

    private void updateGridViewPiezas(){
        AdaptadorCasillas adaptadorPiezas = new AdaptadorCasillas(this, piezas, R.layout.item_casilla, TypeAdapter.Piezas, listener);
        GridView tablero = (GridView) findViewById(R.id.tablero);
        tablero.setAdapter(adaptadorPiezas);
    }

    private ArrayList<Integer> generarCasillas(){
        int colorClaro = prefs.getInt("colorClaro", Color.WHITE);
        int colorOscuro = prefs.getInt("colorOscuro", Color.BLACK);

        ArrayList<Integer> content = new ArrayList<Integer>();

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                content.add((x+1)%2 == (y+1)%2?colorClaro : colorOscuro);
            }
        }
        return content;
    }

    private ArrayList<Integer> generarPiezas(){
        int piezaClara = prefs.getInt("piezaClara", R.drawable.ficha_blanca);
        int piezaOscura = prefs.getInt("piezaOscura", R.drawable.ficha_roja);
        ArrayList<Integer> content = new ArrayList<Integer>();

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                int pieza = 0;
                if(x<=2 && y%2 != x%2){
                    pieza = piezaOscura;
                }
                else if(x>=5 && y%2 != x%2){
                    pieza = piezaClara;
                }
                content.add(pieza);
            }
        }

        return content;
    }

    public void generarPiezas(Long[][] tablero){
        int piezaClara = prefs.getInt("piezaClara", R.drawable.ficha_blanca);
        int piezaOscura = prefs.getInt("piezaOscura", R.drawable.ficha_roja);

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                int pieza = tablero[x][y] % 2 == 0? piezaOscura : piezaClara;
                if(tablero[x][y] == 0) pieza = 0;
                piezas.set(x*8+y, pieza);
                System.out.print(tablero[x][y] + " ");
            }
            System.out.println();
        }
        updateGridViewPiezas();
    }

    //======================================= FUNCIONES PINTAR =======================================

    public void pintarCasilla(int x){
        this.fondos.set(x, prefs.getInt("colorClaroOscuro", Color.GRAY));
        updateGridViewCasillas();
    }

    public void despintarCasillas(){
        this.fondos = generarCasillas();
        updateGridViewCasillas();
    }

    //======================================= FUNCIONES SETTER =======================================

    public void setListener(ListenerPartidaDamas listener){
        this.listener = listener;
    }

    //======================================= FUNCIONES PIEZA =======================================

    public void moverPieza(int a, int b){
        this.soundManager.setSound(R.raw.pressefect, this.context);
        this.soundManager.start();
        int valorA = this.piezas.get(a);
        this.piezas.set(a, this.piezas.get(b));
        this.piezas.set(b, valorA);
        updateGridViewPiezas();

    }

    public void comerPieza(int a){
        this.piezas.set(a, 0);
        updateGridViewPiezas();
    }

    //======================================= FUNCIONES VISUALES =======================================

    public void sumarPunto(boolean turn){

    }

    public void mostrarTurno(){

    }

    //======================================= FUNCIONES SONICAS =======================================

    public void sonidoCasillaVacia(){

    }

    //====================================== LIFE CYCLES ================================================
    @Override
    protected void onDestroy() {
        this.soundManager = null;
        super.onDestroy();
    }
}