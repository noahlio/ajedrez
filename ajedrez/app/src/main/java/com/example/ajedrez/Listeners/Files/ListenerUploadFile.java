package com.example.ajedrez.Listeners.Files;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

public class ListenerUploadFile implements View.OnClickListener {

    private Activity activity;
    public ListenerUploadFile(Activity activity){
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try{
            activity.startActivityForResult(Intent.createChooser(intent, "Selecciona un archivo"), 100);
        }
        catch (Exception exception){
            Toast.makeText(activity, "Instale un administrador de archivos", Toast.LENGTH_SHORT).show();
        }
    }


}
