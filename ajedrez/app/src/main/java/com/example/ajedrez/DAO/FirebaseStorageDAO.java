package com.example.ajedrez.DAO;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ajedrez.Enums.TypeFile;
import com.example.ajedrez.Listeners.Files.ListenerDownloadImageSuccess;
import com.example.ajedrez.Listeners.Toast.ListenerFailureToast;
import com.example.ajedrez.Listeners.Toast.ListenerSuccessToast;
import com.google.android.gms.tasks.Task;
import com.google.common.io.Files;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.Set;

public class FirebaseStorageDAO {

    private FirebaseDatabase db;
    private Context context;
    private static Activity activity;
    private static SharedPreferences prefs;

    public FirebaseStorageDAO(Activity activity){
        this.db = FirebaseDatabase.getInstance();
        this.context = activity.getApplicationContext();
        this.activity = activity;
        this.prefs = context.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
    }

    public void uploadFile(Uri uri, String name, String successMessage, String failureMessage){
        String fileName = getFileName(uri);
        TypeFile typeFile = getTypeFile(fileName);
        File file = new File(fileName);
        String idUsuario = prefs.getString("idUsuario", "guest");
        if(typeFile == null || idUsuario == null){
            Toast.makeText(context, "No se ha podido subir la imagen", Toast.LENGTH_SHORT).show();
            return;
        }
        StorageReference folderReference = FirebaseStorage.getInstance().getReference().child(typeFile.toString());
        StorageReference fileNameReference = folderReference.child(idUsuario);
        UploadTask task = fileNameReference.putFile(uri);
        task.addOnSuccessListener(new ListenerSuccessToast(context, successMessage));
        task.addOnFailureListener(new ListenerFailureToast(context, failureMessage));
    }

    public static void downloadFile(TypeFile typeFile, ImageButton image){
        String idUsuario = prefs.getString("idUsuario", "guest");
        String fileName = "fotoperfil.png";

        StorageReference folderReference = FirebaseStorage.getInstance().getReference().child(typeFile.toString());
        StorageReference fileNameReference = folderReference.child(idUsuario);
        Task<Uri> task = fileNameReference.getDownloadUrl();
        task.addOnSuccessListener(new ListenerDownloadImageSuccess(activity, image, fileName));
    }

    private TypeFile getTypeFile(String fileName){
        Set<String> extensionesImagen = Set.of("png", "jpg", "jpeg");
        Set<String> extensionesAudio = Set.of("mp4", "mpg4", "mp3", "wav");
        String extension = Files.getFileExtension(fileName.toLowerCase());
        if(extensionesImagen.contains(extension)) return TypeFile.fotoPerfil;
        if(extensionesAudio.contains(extension)) return TypeFile.audio;
        return null;
    }

    @SuppressLint("Range")
    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

}
