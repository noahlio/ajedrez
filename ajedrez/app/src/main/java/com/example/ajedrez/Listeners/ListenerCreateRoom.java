package com.example.ajedrez.Listeners;

import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.ajedrez.Activities.JugarActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class ListenerCreateRoom implements ValueEventListener {

    private JugarActivity jugarActivity;
    private TextView viewValor;
    public ListenerCreateRoom(JugarActivity jugarActivity, TextView viewValor){
        this.jugarActivity = jugarActivity;
        this.viewValor = viewValor;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot snapshot) {
        Toast.makeText(jugarActivity, "funciona", Toast.LENGTH_SHORT).show();
        DatabaseReference databaseReference = snapshot.getRef();
        databaseReference.child("clave2").setValue("Hola");
        final String valor = snapshot.child("/clave").getValue(String.class);
        viewValor.setText(valor);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError error) {
        Toast.makeText(jugarActivity, "Fallo de conexion", Toast.LENGTH_SHORT).show();
    }
}