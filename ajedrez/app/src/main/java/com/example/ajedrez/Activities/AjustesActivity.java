package com.example.ajedrez.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.proto.ProtoOutputStream;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajedrez.DAO.FirebaseStorageDAO;
import com.example.ajedrez.Enums.TypeAdapter;
import com.example.ajedrez.Idioma.LocaleHelper;
import com.example.ajedrez.Listeners.ListenerActivity;
import com.example.ajedrez.Listeners.ListenerPieza;
import com.example.ajedrez.R;
import com.example.ajedrez.Sonido.SoundtrackManager;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AjustesActivity extends MyActivity {
    private Context context;
    private Resources resources;
    private ImageButton imggoback;
    private RadioGroup idiomas;
    private RadioButton ingles, castellano;
    private SeekBar volumen;
    private TextView titulo, estilo, colorJ1, colorJ2, visualizacion, idiomaselector;
    private Spinner fichasJ1, fichasJ2;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editorpreferencies;
    private Button selectSong;
    private Button playsnd;
    private Button stopsnd;
    String[] NombresFichasJ1;
    String[] NombresFichasJ2;
    private FirebaseStorageDAO firebaseStorageDAO;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);
        //Al cargar la activity pillar el idioma que hay en preferencias guardado
        context = LocaleHelper.setLocale(AjustesActivity.this,LocaleHelper.getLanguage(AjustesActivity.this));
        resources =context.getResources();
        prefs = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editorpreferencies = prefs.edit();
        initElements();
        onLanguageChangeOrLoad();
        setLanguage();
    }

    private void initElements(){
        this.firebaseStorageDAO = new FirebaseStorageDAO(this);
        this.imggoback = (ImageButton) findViewById(R.id.img_goback);
        this.imggoback.setImageResource(R.drawable.back_arrow);
        this.imggoback.setOnClickListener(new ListenerActivity(this, MainActivity.class, getApplicationContext(),true));
        this.idiomas = findViewById(R.id.lang_selector);
        this.castellano = findViewById(R.id.lang_es);
        this.ingles = findViewById(R.id.lang_en);
        this.titulo = findViewById(R.id.jugar);
        this.estilo = findViewById(R.id.local);
        this.colorJ1 = findViewById(R.id.color_figurasj1);
        this.colorJ2 = findViewById(R.id.color_figurasj2);
        this.playsnd = findViewById(R.id.Music_play);
        this.playsnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SoundtrackManager.class);
                startService(intent);
                editorpreferencies.putBoolean("playMusic", true);
                editorpreferencies.apply();
            }
        });
        this.stopsnd = findViewById(R.id.Music_stop);
        this.stopsnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SoundtrackManager.class);
                stopService(intent);
                editorpreferencies.putBoolean("playMusic", false);
                editorpreferencies.apply();
            }
        });
        this.selectSong = findViewById(R.id.selectSong);
        this.selectSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/mpeg");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try{
                    startActivityForResult(Intent.createChooser(intent, "Selecciona un archivo"), 100);
                }
                catch (Exception exception){
                    Toast.makeText(view.getContext(), "Instale un administrador de archivos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.NombresFichasJ1 = new String[]{resources.getText(R.string.white).toString(), resources.getText(R.string.blue).toString(), resources.getText(R.string.green).toString()};
        int[] imagenesFichasJ1 = new int[]{R.drawable.ficha_blanca, R.drawable.ficha_azulclaro, R.drawable.ficha_verdeclaro};
        this.fichasJ1 = (Spinner) findViewById(R.id.spinner_figuras_j1);
        ArrayAdapter<String> adaptadorImagenesJ1 = new ArrayAdapter(this, R.layout.item_text, NombresFichasJ1);
        this.fichasJ1.setAdapter(adaptadorImagenesJ1);
        this.fichasJ1.setOnItemSelectedListener(listenerSelected("piezaClara", imagenesFichasJ1));
        for(int x =0; x<imagenesFichasJ1.length;x++)
            if(prefs.getInt("piezaClara",0) == imagenesFichasJ1[x])
                fichasJ1.setSelection(x);

        this.NombresFichasJ2 = new String[]{resources.getText(R.string.red).toString(), resources.getText(R.string.dark_blue).toString(), resources.getText(R.string.dark_green).toString()};
        int[] imagenesFichasJ2 = new int[]{R.drawable.ficha_roja, R.drawable.ficha_azuloscuro, R.drawable.ficha_verdeoscuro};
        this.fichasJ2 = (Spinner) findViewById(R.id.spinner_figuras_j2);
        ArrayAdapter<String> adaptadorImagenesJ2 = new ArrayAdapter(this, R.layout.item_text, NombresFichasJ2);
        this.fichasJ2.setAdapter(adaptadorImagenesJ2);
        this.fichasJ2.setOnItemSelectedListener(listenerSelected("piezaOscura", imagenesFichasJ2));
        for(int x =0; x<imagenesFichasJ2.length;x++)
            if(prefs.getInt("piezaOscura",0) == imagenesFichasJ2[x])
                fichasJ2.setSelection(x);

        this.visualizacion = findViewById(R.id.display);
        this.idiomaselector = findViewById(R.id.idioma);
        SeekBar volumeSlider = (SeekBar) findViewById(R.id.volume_slider);
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeSlider.setProgress(currentVolume *10);
        volumeSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Do something with the volume value here
                AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                audio.setStreamVolume(AudioManager.STREAM_MUSIC, progress / 10, 0);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Do something when the user starts moving the slider
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Do something when the user stops moving the slider
            }
        });
    }
    public void setLanguage() {
        this.idiomas.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.lang_en:
                        context = LocaleHelper.setLocale(AjustesActivity.this, "en");
                        resources = context.getResources();
                        onLanguageChangeOrLoad();
                        startActivity(getIntent());
                        finish();
                        overridePendingTransition(0,0);
                        break;
                    case R.id.lang_es:
                        context = LocaleHelper.setLocale(AjustesActivity.this, "es");
                        resources = context.getResources();
                        onLanguageChangeOrLoad();
                        startActivity(getIntent());
                        finish();
                        overridePendingTransition(0,0);
                        break;
                }
            }
        });
    }

    public void onLanguageChangeOrLoad() {
        //titulo
        this.titulo.setText(resources.getString(R.string.ajustes));
        //Apartado Estilo
        this.estilo.setText(resources.getString(R.string.options_style));
            //Colores de figuras
        this.colorJ1.setText(resources.getText(R.string.options_style_figureColor1));
        this.colorJ2.setText(resources.getText(R.string.options_style_figureColor2));

        //Apartado visualización
        this.visualizacion.setText(resources.getString(R.string.options_display));
            //Casillas de lenguaje
        this.ingles.setText(resources.getString(R.string.lang_en));
        this.castellano.setText(resources.getString(R.string.lang_es));
        this.idiomaselector.setText(resources.getString(R.string.lang));
        this.NombresFichasJ2[0] = resources.getText(R.string.red).toString();
        this.NombresFichasJ2[1] = resources.getText(R.string.dark_blue).toString();
        this.NombresFichasJ2[2] = resources.getText(R.string.dark_green).toString();
        this.NombresFichasJ1[0] =resources.getText(R.string.white).toString();
        this.NombresFichasJ1[1] =resources.getText(R.string.blue).toString();
        this.NombresFichasJ1[2] =resources.getText(R.string.green).toString();
        this.stopsnd.setText(resources.getText((R.string.btn_stop)));
        this.playsnd.setText(resources.getText((R.string.btn_play)));
    }

    private AdapterView.OnItemSelectedListener listenerSelected(String pieza, int[] imagenesFichas){
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                editorpreferencies.putInt(pieza,imagenesFichas[i]);
                editorpreferencies.apply();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        };
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == 100 && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();

            this.firebaseStorageDAO.uploadFile(uri, "musicaFondo",
                    "Musica de fondo actualizada correctamente",
                    "No se ha podido subir la musica de fondo");
        }
    }
}
