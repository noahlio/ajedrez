package com.example.ajedrez.Listeners.Firebase;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.ajedrez.Activities.Partidas.PartidaActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivityDamas;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.VO.Juegos;
import com.example.ajedrez.VO.PartidaDamas;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class ListenerReadPartidaEvent implements EventListener<DocumentSnapshot> {

    private FirebaseDAO firebaseDAO;
    private PartidaActivity partidaActivity;

    public ListenerReadPartidaEvent(FirebaseDAO firebaseDAO, PartidaActivity partidaActivity){
        this.firebaseDAO = firebaseDAO;
        this.partidaActivity = partidaActivity;
    }

    @Override
    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
        Long idJuego = firebaseDAO.getIdJuego();
        this.getPartidaDamas(idJuego, value);
    }

    private void getPartidaDamas(Long idJuego, DocumentSnapshot value){
        if(!idJuego.equals(Juegos.DAMAS)) return;
        PartidaDamas partida = firebaseDAO.getPartidaDamas(value);
        PartidaActivityDamas partidaActivityDamas = (PartidaActivityDamas) partidaActivity;
        partidaActivityDamas.getPartidaOnline(partida);
    }
}
