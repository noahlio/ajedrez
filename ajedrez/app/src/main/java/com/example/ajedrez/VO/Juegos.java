package com.example.ajedrez.VO;

import android.graphics.Color;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Juegos {
    public static final Long DAMAS = Long.valueOf(0);
    public static final Long AJEDREZ = Long.valueOf(1);
    public static final Map<Long, Integer> JUGADORES = new HashMap<Long, Integer>(){
        {
            put(DAMAS, 2);
            put (AJEDREZ, 2);
        }
    };
}
