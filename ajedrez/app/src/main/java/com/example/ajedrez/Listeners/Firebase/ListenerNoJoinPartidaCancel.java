package com.example.ajedrez.Listeners.Firebase;

import android.app.SearchManager;
import android.content.DialogInterface;

import androidx.annotation.NonNull;

import com.example.ajedrez.VO.Juegos;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.function.Consumer;

public class ListenerNoJoinPartidaCancel implements DialogInterface.OnCancelListener {

    private boolean cancelled;
    private Consumer<Boolean> removeGame;

    public ListenerNoJoinPartidaCancel(Consumer<Boolean> removeGame){
        this.cancelled = false;
        this.removeGame = removeGame;
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        this.removeGame.accept(true);
        this.cancelled = true;
    }

    public boolean isCancelled(){
        return this.cancelled;
    }
}
