package com.example.ajedrez.Listeners;

import android.view.View;

import com.example.ajedrez.Listeners.Partidas.ListenerPartidaDamas;

public class ListenerPieza{
    private View.OnClickListener onClickListener;
    private ListenerPartidaDamas listener;
    public ListenerPieza(ListenerPartidaDamas listener, int posicion){
        this.onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.seleccionar(posicion);
            }
        };
    }
    public View.OnClickListener getOnClickListener(){
        return this.onClickListener;
    }
}