package com.example.ajedrez.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.KeyEvent;

public class MyActivity extends Activity {

    public void onDestroy(Intent intent){

        this.finish();
        this.startActivity(intent);
    }
}
