package com.example.ajedrez.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.ajedrez.Activities.Partidas.PartidaActivityDamas;
import com.example.ajedrez.Adaptadores.AdaptadorListaJuegos;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.Idioma.LocaleHelper;
import com.example.ajedrez.Listeners.ListenerActivity;
import com.example.ajedrez.VO.Juegos;
import com.example.ajedrez.VO.Partida;
import com.example.ajedrez.VO.PartidaDamas;
import com.example.ajedrez.R;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JugarActivity extends MyActivity {

    private ImageView img1, img2, img3, img4;
    private ImageButton imggoback;
    private Button btnPlayLocal, btnCreateRoom;
    private Context context;
    private Resources resources;
    private EditText nombresala, contrasala;
    private Button crearsala, cancelarsala, playlocal, hostroom;
    private TextView jugar, local, online, titulomodal, nombresalamodal, contrasalamodal;
    private String id;

    private SharedPreferences prefs;
    private FirebaseDAO firebaseDAO;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugar);
        initElements();

        //Fondo degradado
        ConstraintLayout constraintLayout = findViewById(R.id.bg);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(500);
        animationDrawable.setExitFadeDuration(2500);
        animationDrawable.start();

        onLanguageChangeOrLoad();
    }


    private void initElements(){
        this.prefs  = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        firebaseDAO = new FirebaseDAO(prefs.getLong("Juego", Juegos.DAMAS));
        //Guardar la activity para pasarsela a los pop-up
        this.btnPlayLocal = (Button) findViewById(R.id.btn_playLocal);
        this.btnPlayLocal.setOnClickListener(new ListenerActivity(this, PartidaActivityDamas.class, getApplicationContext(), false));

        this.img1 = (ImageView) findViewById(R.id.img_local_left);
        this.img1.setImageResource(R.drawable.jugadorblanco);
        this.img2 = (ImageView) findViewById(R.id.img_local_right);
        this.img2.setImageResource(R.drawable.jugadorrojo);
        this.img3 = (ImageView) findViewById(R.id.img_online_row1_left);
        this.img3.setImageResource(R.drawable.damaroja);
        this.img4 = (ImageView) findViewById(R.id.img_online_row1_right);
        this.img4.setImageResource(R.drawable.damaroja);

        this.imggoback = (ImageButton) findViewById(R.id.img_goback);
        this.imggoback.setImageResource(R.drawable.back_arrow);
        this.imggoback.setOnClickListener(new ListenerActivity(this, MainActivity.class, getApplicationContext(), true));

        this.playlocal = findViewById(R.id.btn_playLocal);
        this.hostroom = findViewById(R.id.btn_createRoom);
        this.jugar = findViewById(R.id.jugar);
        this.local = findViewById(R.id.local);
        this.online = findViewById(R.id.play_online);

        this.btnCreateRoom = (Button) findViewById(R.id.btn_createRoom);
        this.btnCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crearSala();
            }
        });

        //Spinner
        final List<String> juegos = Arrays.asList("DAMAS", "AJEDREZ");
        final Spinner spinnerjuegos = findViewById(R.id.spinner);

        AdaptadorListaJuegos adaptador = new AdaptadorListaJuegos(getApplicationContext(), juegos);
        adaptador.setDropDownViewResource(R.layout.spinner_game_selected);

        spinnerjuegos.setAdapter(adaptador);
        spinnerjuegos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Hacer metodo que dependiendo de la pos sea otro juego en sharedpreferences
                setJuego(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        //Cargar strings idioma
        this.context = LocaleHelper.setLocale(JugarActivity.this,LocaleHelper.getLanguage(JugarActivity.this));
        this.resources =context.getResources();
    }

    public void onLanguageChangeOrLoad() {
        this.jugar.setText(resources.getString(R.string.jugar));
        this.online.setText(resources.getString(R.string.play_online));
        this.local.setText(resources.getString(R.string.play_local));

        this.playlocal.setText(resources.getString(R.string.start_match_local));
        this.hostroom.setText(resources.getString(R.string.create_match));
    }

    public void crearSala(){
        this.firebaseDAO.searchGameToJoin("partidas", this);
    }

    public void setId(String id){
        this.id = id;
    }

    public void unirSala(Partida partida){
        if(partida.getIdJuego().equals(Juegos.DAMAS)){
            unirSalaDamas((PartidaDamas) partida);
        }
    }

    private void unirSalaDamas(PartidaDamas partidaDamas){
        ProgressDialog waitingmsg = new ProgressDialog(this);

        String nombre = prefs.getString("NOM", "Guest");
        if (partidaDamas.getJ1().equals("")) {
            Log.d("ENTRA", "unirSala: " + partidaDamas.getId());
            partidaDamas.setJ1(nombre);
            firebaseDAO.updateCollection("partidas", partidaDamas, bool -> {});
            Intent intent = new Intent(this, PartidaActivityDamas.class);
            waitingmsg.setCancelable(true);
            waitingmsg.setMessage(resources.getString(R.string.modal_waiting));
            waitingmsg.show();
            firebaseDAO.FindCollection("partidas",partidaDamas.getId(),waitingmsg, this);
        }
        else {
            firebaseDAO.FindWaitingMatch("partidas", nombre);
            waitingmsg.dismiss();
            partidaDamas.setJ2(nombre);
            //Falta actualizar datos???
            Intent intent = new Intent(this, PartidaActivityDamas.class);
            intent.putExtra("id", partidaDamas.getId());
            intent.putExtra("j1", partidaDamas.getJ1());
            intent.putExtra("j2", nombre);
            intent.putExtra("jugador", "j2");
            startActivity(intent);
        }
    }

    private void setJuego(int idjuego) {
        SharedPreferences.Editor editorprefs = prefs.edit();
        if (idjuego == 0) {
            editorprefs.putLong("Juego", Juegos.DAMAS);

        }
        else if (idjuego == 1) {
            editorprefs.putLong("Juego", Juegos.AJEDREZ);
        }
        editorprefs.commit();
        firebaseDAO = new FirebaseDAO(prefs.getLong("Juego", Juegos.DAMAS));
    }
}