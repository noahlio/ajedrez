package com.example.ajedrez.Listeners;

import android.view.View;

public class ListenerCounter implements View.OnClickListener {

    private View view;
    private int count;

    public ListenerCounter(View view){
        this.view = view;
    }
    public ListenerCounter(View view, int initialCount){
        this(view);
        this.count = initialCount;
    }

    @Override
    public void onClick(View view) {
        this.count++;
    }
}
