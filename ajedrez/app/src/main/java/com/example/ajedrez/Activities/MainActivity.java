package com.example.ajedrez.Activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ajedrez.Idioma.LocaleHelper;
import com.example.ajedrez.Listeners.ListenerActivity;
import com.example.ajedrez.Listeners.ListenerDestroy;
import com.example.ajedrez.R;
import com.example.ajedrez.Sonido.Services.SoundManager;
import com.example.ajedrez.Sonido.SoundtrackManager;

public class MainActivity extends MyActivity {
    private Context context;
    private Resources resources;
    private ImageButton btnMenuJugar;
    private ImageButton btnMenuAjustes;
    private ImageButton btnMenuPerfil;
    private ImageButton btnSalida;
    private TextView titulo, desarrolladora, saludo, mensaje;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editorsp;
    private ImageView fondo;
    private Animation bganim, greetings;
    private LinearLayout textobienvenida, loadedtxtbienvenida,optionslayout;
    private Intent intentSoundtrackManager;
    private SoundManager sndmanager;
    private static boolean startedbgm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initElements();
        context = LocaleHelper.setLocale(MainActivity.this,LocaleHelper.getLanguage(MainActivity.this));
        resources =context.getResources();
        onLanguageChangeOrLoad();
        CheckOrGenerateStats();
    }

    private void initElements(){
        //Falta poner cardviews a las imagenes
        //Animaciones

        this.optionslayout = findViewById(R.id.options_layout);

        intentSoundtrackManager = new Intent(this, SoundtrackManager.class);

        this.btnMenuJugar = findViewById(R.id.btnMenuJugar);
        ListenerActivity listenerActivity = new ListenerActivity(this, JugarActivity.class, getApplicationContext(),false);
        this.btnMenuJugar.setOnClickListener(listenerActivity);

        this.btnMenuAjustes = findViewById(R.id.btnMenuAjustes);
        listenerActivity = new ListenerActivity(this, AjustesActivity.class, getApplicationContext(), false);
        this.btnMenuAjustes.setOnClickListener(listenerActivity);

        this.btnMenuPerfil = findViewById(R.id.btnMenuPerfil);

        this.btnMenuJugar.setEnabled(false);
        this.btnMenuAjustes.setEnabled(false);
        this.btnMenuPerfil.setEnabled(false);

        listenerActivity = new ListenerActivity(this, PerfilActivity.class, getApplicationContext(),false);
        this.btnMenuPerfil.setOnClickListener(listenerActivity);

        //this.btnImagenInicio = findViewById(R.id.btnImagenInicio);
        //ListenerCounter listenerCount = new ListenerCounter(this.btnImagenInicio);
        //this.btnImagenInicio.setOnClickListener(listenerCount);

        this.titulo = findViewById(R.id.game_title);

        this.btnSalida = findViewById(R.id.btnSalida);
        ListenerDestroy listenerDestroy = new ListenerDestroy(this);
        this.btnSalida.setOnClickListener(listenerDestroy);

        this.prefs = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        this.editorsp = prefs.edit();

        //Fondo
        this.fondo = findViewById(R.id.bganimed);

        //Mensaje inicial
        this.textobienvenida = findViewById(R.id.welcome_text);
        this.mensaje = findViewById(R.id.goodmrng_subtitle);
        this.saludo = findViewById(R.id.goodmrng);
        this.desarrolladora = findViewById(R.id.wiber_developed);

        this.sndmanager = new SoundManager();
        if (this.prefs.getBoolean("playMusic", true)) {
            startService(intentSoundtrackManager);
        }
        //Mensaje arriba izda (Oculto)
        this.loadedtxtbienvenida = findViewById(R.id.loaded_wtext);

        if (this.prefs.getBoolean("startupAnim", true)) {
            this.sndmanager.setSound(R.raw.startupsound, this.getApplicationContext());
            this.sndmanager.start();
            this.greetings = AnimationUtils.loadAnimation(this, R.anim.greetingsanim);
            this.optionslayout.startAnimation(greetings);
            this.bganim = AnimationUtils.loadAnimation(this,R.anim.bganim);
            this.fondo.animate().translationY(-2000).setDuration(1500).setStartDelay(500);
            this.textobienvenida.animate().translationY(140).alpha(0).setDuration(800).setStartDelay(500);
            this.loadedtxtbienvenida.startAnimation(greetings);
            this.editorsp.putBoolean("startupAnim", false).commit();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //A los 2 segundos habilitar los botones
                    btnMenuJugar.setEnabled(true);
                    btnMenuAjustes.setEnabled(true);
                    btnMenuPerfil.setEnabled(true);
                }
            },1800);
        }

        else {
            this.btnMenuJugar.setEnabled(true);
            this.btnMenuAjustes.setEnabled(true);
            this.btnMenuPerfil.setEnabled(true);
            this.greetings = AnimationUtils.loadAnimation(this, R.anim.greetingsanim);
            this.bganim = AnimationUtils.loadAnimation(this,R.anim.bganim);
            this.fondo.animate().translationY(-2000).setDuration(0).setStartDelay(0);
            this.textobienvenida.animate().translationY(140).alpha(0).setDuration(0).setStartDelay(0);
        }

    }

    public void onLanguageChangeOrLoad() {
        this.desarrolladora.setText(resources.getText(R.string.developed_by));
        this.mensaje.setText(resources.getText(R.string.welcome_user_mssg));
        this.saludo.setText(resources.getText(R.string.welcome_user) + ", " + prefs.getString("NOM", "Guest"));
        this.titulo.setText(resources.getText(R.string.app_name));
        //this.btnMenuJugar.setText(resources.getText(R.string.jugar));
        //this.btnMenuAjustes.setText(resources.getText(R.string.ajustes));
    }

    public void CheckOrGenerateStats() {
        if (!prefs.contains("Victories")) {
            editorsp.putInt("Victories",0);
            editorsp.apply();
        }
        if (!prefs.contains("Derrotes")) {
            editorsp.putInt("Derrotes",0);
            editorsp.apply();
        }
        Log.d("TAG", "CheckOrGenerateStats:" + prefs.getInt("Victories",0));
        Log.d("TAG", "CheckOrGenerateStats:" + prefs.getInt("Derrotes",0));
    }
}