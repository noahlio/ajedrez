package com.example.ajedrez.Listeners;

import android.app.AlertDialog;
import android.view.View;

import com.example.ajedrez.Activities.MyActivity;

import android.content.res.Resources;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.*;
import java.util.function.Consumer;

public class ListenerModal implements View.OnClickListener {

    private MyActivity activity;
    private Resources resources;
    private int idModal;
    private int idTitulo;
    private String titulo;
    private int idAceptar;
    private String aceptarText;
    private int idCancelar;
    private String cancelarText;
    private int[] idsElementos;
    private Consumer<List<String>> handlerAccept;
    private Consumer<Boolean> handlerCancel;
    private String[] valores;

    public ListenerModal(MyActivity activity, int idModal, int idTitulo, String titulo,
    int idAceptar, String aceptarText, int idCancelar, String cancelarText,
    int[] idsElementos, String[] valores, Consumer<List<String>> handlerAccept, Consumer<Boolean> handlerCancel){
        this.activity = activity;
        this.idModal = idModal;
        this.idTitulo = idTitulo;
        this.titulo = titulo;
        this.idAceptar = idAceptar;
        this.aceptarText = aceptarText;
        this.idCancelar = idCancelar;
        this.cancelarText = cancelarText;
        this.idsElementos = idsElementos;
        this.handlerAccept = handlerAccept;
        this.handlerCancel = handlerCancel;
        this.valores = valores;
    }
    @Override
    public void onClick(View view) {
        showModal();
    }
    public void showModal(){
        this.resources = activity.getResources();
        final View editName = activity.getLayoutInflater().inflate(idModal, null);

        for(int x = 0; x<idsElementos.length; x++){
            EditText text = (EditText) editName.findViewById(idsElementos[x]);
            text.setText(valores[x]);
        }

        AlertDialog.Builder builderDialogo = new AlertDialog.Builder(activity);
        builderDialogo.setView(editName);
        AlertDialog dialogo = builderDialogo.create();
        dialogo.show();

        TextView viewTitulo = editName.findViewById(idTitulo);
        viewTitulo.setText(titulo);

        Button aceptar = editName.findViewById(idAceptar);
        aceptar.setText(aceptarText);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> valores = new ArrayList<String>();
                for(int elemento : idsElementos){
                    EditText text = (EditText) editName.findViewById(elemento);
                    valores.add(text.getText().toString());
                }
                handlerAccept.accept(valores);
                dialogo.dismiss();
            }
        });

        Button cancelar = editName.findViewById(idCancelar);
        cancelar.setText(cancelarText);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handlerCancel.accept(true);
                dialogo.dismiss();
            }
        });
    }
}
