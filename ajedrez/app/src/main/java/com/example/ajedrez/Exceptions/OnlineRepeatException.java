package com.example.ajedrez.Exceptions;

import java.io.IOException;

public class OnlineRepeatException extends IOException {
    public OnlineRepeatException() {
        super("Esta jugada ya ha pasado");
    }

    public OnlineRepeatException(String s) {
        super(s);
    }
}
