package com.example.ajedrez.Models;

import com.example.ajedrez.Exceptions.OnlineRepeatException;
import com.example.ajedrez.VO.PartidaDamas;

public interface IModelPartida {
    void updatePartidaLocal(PartidaDamas partida) throws OnlineRepeatException;
}
