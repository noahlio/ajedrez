package com.example.ajedrez.Listeners.Firebase;

import androidx.annotation.NonNull;

import com.example.ajedrez.VO.Juegos;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListenerFindPartidaComplete implements OnCompleteListener<QuerySnapshot> {

    private FirebaseFirestore db;
    private String nombre;
    private int numeroJugadores;

    public ListenerFindPartidaComplete(FirebaseFirestore db, String nombre, Long idJuego){
        this.db = db;
        this.nombre = nombre;
        this.numeroJugadores = Juegos.JUGADORES.get(idJuego);
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        for (QueryDocumentSnapshot document : task.getResult()) {
            if(!faltaJugador(document)) continue;
            unirsePartida(document);
            return;
        }
    }

    private boolean faltaJugador(QueryDocumentSnapshot document){
        switch (numeroJugadores){
            case 2:
                return faltaJugador2Jugadores(document);

            default:
                return false;
        }
    }

    private boolean faltaJugador2Jugadores(QueryDocumentSnapshot document){
        return document.getData().get("j2").equals("");
    }

    private void unirsePartida(QueryDocumentSnapshot document){
        String idPartida = document.getId();
        DocumentReference partida = db.collection("partidas").document(idPartida);
        partida.update("j2", nombre);
    }
}
