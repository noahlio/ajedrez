package com.example.ajedrez.Listeners.Partidas;

import android.util.Log;

import com.example.ajedrez.Activities.Partidas.PartidaActivityDamas;
import com.example.ajedrez.Exceptions.EmptyBoxException;
import com.example.ajedrez.Exceptions.OnlineRepeatException;
import com.example.ajedrez.Exceptions.WrongTurnException;
import com.example.ajedrez.Models.ModelPartidaDamas;
import com.example.ajedrez.VO.PartidaDamas;

import java.util.List;
import java.util.Map;

public class ListenerPartidaDamas extends ListenerPartida implements IListenerPartida, IListenerPartidaPiezas, IListenerPartidaTablero{

    private ModelPartidaDamas model;
    private PartidaActivityDamas view;

    public ListenerPartidaDamas(ModelPartidaDamas model, PartidaActivityDamas view){
        this.view = view;
        this.model = model;
    }

    //======================================= FUNCIONES INICIO =======================================

    public void init(){
        Log.d("ListenerPartidaDamas", "init: init del model");
        this.model.init();
    }

    //======================================= FUNCIONES SELECCION =======================================

    public void seleccionar(int posicion){
        Log.d("ListenerPartidaDamas", "seleccionar: despintar casillas anteriores pintar a las que puede ir la seleccionada");
        try{
            this.view.despintarCasillas();
            this.model.seleccionarCasilla(this.parseToTwoDimension(posicion));
        }
        catch (EmptyBoxException ebe){
            Log.e("EmptyBox", "seleccionarCasilla: casilla vacia");
            this.view.mostrarTurno();
        }
        catch (WrongTurnException wte){
            Log.e("WrongTurn", "seleccionarCasilla: no es tu turno");
            this.view.sonidoCasillaVacia();
        }
    }

    //======================================= FUNCIONES ONLINE =======================================

    public void getPartidaOnline(PartidaDamas partida) throws OnlineRepeatException {
        Log.d("ListenerPartidaDamas", "getPartidaOnline: cargar la partida online");
        this.model.updatePartidaLocal(partida);
    }

    public void setPartidaOnline(Map<String, List<?>> jugadas, boolean turn){
        Log.d("ListenerPartidaDamas", "setPartidaOnline: actualizar la partida online");
        this.view.setPartidaOnline(jugadas, !turn);
    }

    //======================================= FUNCIONES PINTAR =======================================

    public void pintarTableroOnline(boolean online, Long[][] tablero){
        if(!online) return;
        Log.d("ListenerPartidaDamas", "pintarTableroOnline: mostrar tablero con los cambios hechos online");
        this.view.generarPiezas(tablero);
    }

    public void pintarCasilla(int a, int b){
        Log.d("ListenerPartidaDamas", "pintarCasilla: pintar casilla en el tablero");
        this.view.pintarCasilla(this.parseToOneDimension(a, b));
    }

    //======================================= FUNCIONES PIEZAS =======================================

    public void moverPieza(int[] c1, int[] c2){
        Log.d("ListenerPartidaDamas", "moverPieza: mover casilla en el tablero");
        this.view.moverPieza(parseToOneDimension(c1[0], c1[1]), parseToOneDimension(c2[0], c2[1]));
    }

    public void comerPieza(int[] posicion, boolean turn){
        Log.d("ListenerPartidaDamas", "comerPieza: quitar casilla del tablero y dar puntos");
        this.view.comerPieza(parseToOneDimension(posicion[0], posicion[1]));
        this.view.sumarPunto(turn);
        this.model.perderPieza(!turn);
    }

    //======================================= FUNCIONES FIN PARTIDA =======================================

    public void finPartida(int resultado){
        Log.d("ListenerPartidaDamas", "finPartida: mostrar en pantalla el ganador");
        this.view.finPartida(resultado);
    }

    //======================================= FUNCIONES TIMER (LOCAL) ======================================
    public void swapTimer(boolean turn) {
        //false == j2
        //true == j1
        this.view.stopTimer(!turn);
        this.view.startTimer(turn);
    }
}
