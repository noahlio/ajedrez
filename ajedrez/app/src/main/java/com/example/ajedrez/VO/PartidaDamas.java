package com.example.ajedrez.VO;

import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PartidaDamas extends Partida {
    private Boolean turn;
    private String J1, J2;

    //======================================= FUNCIONES INIT =======================================

    public PartidaDamas(Partida partida){
        super(partida);
        this.init();
    }

    public PartidaDamas(){
        super();
        this.init();
    }

    public PartidaDamas(String J1){
        this();
        this.J1 = J1;
    }

    private void init(){
        this.J1 = "";
        this.J2 = "";
        this.turn = true;
    }

    //=================================== FUNCIONES COMPROBACION ===================================

    public boolean gameReady(){
        return this.J1.length()>0 && this.J2.length()>0;
    }

    public boolean gameEmpty(){
        return this.J1.equals("") && this.J2.equals("");
    }

    //===================================== FUNCIONES JUGADAS ======================================

    //================================== FUNCIONES GETTER/SETTER ===================================

    public void setJ1(String J1){
        this.J1 = J1;
    }

    public String getJ1(){
        return this.J1;
    }

    public void setJ2(String J2){
        this.J2 = J2;
    }

    public String getJ2(){
        return this.J2;
    }

    public void setTurn(Boolean turn) {
        this.turn = turn;
    }

    public Boolean getTurn() {
        return this.turn;
    }

    //======================================== FUNCIONES TO ========================================

    public Map<String, Object> toMap(){
        Map<String, Object> collection = super.toMap();
        collection.put("j1", this.J1);
        collection.put("j2", this.J2);
        collection.put("turn", this.turn);
        return collection;
    }
}