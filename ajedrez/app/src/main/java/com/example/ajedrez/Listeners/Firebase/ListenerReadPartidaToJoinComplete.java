package com.example.ajedrez.Listeners.Firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ajedrez.Activities.JugarActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivity;
import com.example.ajedrez.Activities.Partidas.PartidaActivityDamas;
import com.example.ajedrez.DAO.FirebaseDAO;
import com.example.ajedrez.VO.Juegos;
import com.example.ajedrez.VO.Partida;
import com.example.ajedrez.VO.PartidaDamas;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListenerReadPartidaToJoinComplete implements OnCompleteListener<QuerySnapshot> {

    private FirebaseDAO firebaseDAO;
    private JugarActivity jugarActivity;
    public ListenerReadPartidaToJoinComplete(FirebaseDAO firebaseDAO, JugarActivity jugarActivity){
        this.firebaseDAO = firebaseDAO;
        this.jugarActivity = jugarActivity;
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        for (QueryDocumentSnapshot document : task.getResult()) {
            Partida partida = firebaseDAO.getPartidaDamas(document);
            if(partida.gameReady()) continue;
            jugarActivity.unirSala(partida);
            return;
        }

        crearPartida(jugarActivity);
    }

    private void crearPartida(JugarActivity jugarActivity){
        Long idJuego = firebaseDAO.getIdJuego();
        crearPartidaDamas(idJuego, jugarActivity);
    }

    private void crearPartidaDamas(Long idJuego, JugarActivity jugarActivity){
        if(!idJuego.equals(Juegos.DAMAS)) return;
        PartidaDamas myPartida = new PartidaDamas();
        myPartida.setIdJuego(Juegos.DAMAS);
        try {
            firebaseDAO.insertInCollection(myPartida, "partidas", jugarActivity);
        } catch (InterruptedException e) {
            jugarActivity.crearSala();
        }
    }
}
