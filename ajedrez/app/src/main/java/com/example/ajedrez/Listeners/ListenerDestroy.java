package com.example.ajedrez.Listeners;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;

import com.example.ajedrez.Activities.MyActivity;

public class ListenerDestroy implements View.OnClickListener {

    private MyActivity myActivity;
    private SharedPreferences prefs;
    private android.content.SharedPreferences.Editor editorsp;

    public ListenerDestroy(MyActivity myActivity){
        this.myActivity = myActivity;
    }
    @Override
    public void onClick(View view) {
        myActivity.finishAffinity();
        this.prefs = myActivity.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        this.editorsp = prefs.edit();
        this.editorsp.putBoolean("startupAnim", true);
        this.editorsp.commit();
    }
}