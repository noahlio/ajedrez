package com.example.ajedrez.Activities.Partidas;

public interface IPartidaActivity {
    void onLanguageChangeOrLoad();
    void mostrarTurno();
    void sonidoCasillaVacia();
    void finPartida(int resultado);
    void setWinOrLose(int resultado);
}
