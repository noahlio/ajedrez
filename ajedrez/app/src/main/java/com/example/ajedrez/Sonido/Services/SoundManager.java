package com.example.ajedrez.Sonido.Services;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;

import com.example.ajedrez.R;

public class SoundManager {

	private Context pContext;
	private MediaPlayer md;
	private int soundID;

	public void setSound(int songid, Context contexto) {
		md = MediaPlayer.create(contexto, songid);
		this.soundID = songid;
	}

	public void start() {
		md.start();
	}

	public void stop() {
		md.stop();
		md.deselectTrack(this.soundID);
		md.release();
	}

	public void pause() {
		md.pause();
	}

	public void loop() {
		md.setLooping(true);
	}

	public void stopLoop() {
		md.setLooping(false);
	}




}
